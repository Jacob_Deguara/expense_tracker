/* credit towards;
    > 'for' part of the 'Create' : https://code-boxx.com/shopping-list-vanilla-javascript/#sec-download
    >
*/ 
var etrack = {

    // Initilize The Expenditure
    expenses : [], //Current expenses
    hList : null, //HTML list
    hAdd : null, //HTML add exp form
    hExpense: null, //HTML add exp input field
    hAmount: null, //HTML add amount input field
    hEditId : null, //Edit id 
    hTotal: null, //Amount total to be outputed
    
    //Startup function
    init : function () {
        // Get HTML Elements 
        etrack.hList = document.getElementById('exp-list');
        etrack.hAdd = document.getElementById('exp-add');
        etrack.hExpense = document.getElementById('exp-item');
        etrack.hAmount = document.getElementById('exp-amount');
        //button trackers
        document.getElementById('exp-add-btn').addEventListener('click',etrack.add);
        document.getElementById('exp-edit-btn').addEventListener('click',etrack.editUnits);

        //load expenses from local server
        if(localStorage.expenses == undefined){localStorage.expenses = "[]";}
        etrack.expenses = JSON.parse(localStorage.expenses);

        //Create HTML Expendicture List
        etrack.create();

        //removeing edit options
        document.getElementById("exp-edit-btn").style.display="none";
        document.getElementById("exp-edit").style.display="none";

    },

    //add new expediture to list
    add :function (evt){

        //Prevent form sumbit
        evt.preventDefault();
        
        //checking if amount is num before saving
        if(isNaN(etrack.hAmount.value)){
            alert('amount is not a number!');
        } else{
            //Adding new 'object' to list
            etrack.expenses.push({
                name : etrack.hExpense.value,
                amount : etrack.hAmount.value
            })
            etrack.hExpense.value = '';
            etrack.hAmount.value = '';
        }
        //save list
        etrack.save();

        // reload List
        etrack.create();
    },

    // this create function is credited towards 
    create : function(){
        etrack.hList.innerHTML ='';
        //checking if expenses is greater then 1
        if (etrack.expenses.length > 0){

            etrack.hTotal = 0;
            let row, name, amount, delbtn, editbtn;
            // for loop for every object in expenses, including buttons for each
            for(let i in etrack.expenses){

                // Rows
                row = document.createElement('div');
                row.className = 'exp-row';
                etrack.hList.appendChild(row); // what is append child?

                // Expenses Names
                name = document.createElement('div');
                name.className ='exp-row';
                name.innerHTML = etrack.expenses[i].name;
                row.appendChild(name);

                //expenses amount
                amount = document.createElement('div');
                amount.className ='exp-row';
                amount.innerHTML = 'EUR ' + etrack.expenses[i].amount;
                row.appendChild(amount);

                // Delete button
                delbtn = document.createElement("input");
                delbtn.className = "exp-del";
                delbtn.type = "button";
                delbtn.value = "Delete";
                delbtn.dataset.id = i;
                delbtn.addEventListener("click", etrack.delete);
                row.appendChild(delbtn); 

                // edit button
                editbtn = document.createElement("input");
                editbtn.className = "exp-edit";
                editbtn.type = "button";
                editbtn.value = "Edit";
                editbtn.dataset.id = i;
                editbtn.addEventListener("click", etrack.edit);
                row.appendChild(editbtn); 

                //adding all expense amounts
                etrack.hTotal += parseFloat(etrack.expenses[i].amount);
            
            }
            //final row for Total Amount
            row = document.createElement('div');
            row.className = 'exp-row';
            etrack.hList.appendChild(row);
            
            name = document.createElement('div');
            name.className ='exp-row';
            name.innerHTML = 'Total Amount';
            row.appendChild(name);
            
            amount = document.createElement('div');
            amount.className ='exp-row';
            amount.innerHTML ='EUR ' + etrack.hTotal;
            row.appendChild(amount);
        }
    },
    //delete function
    delete : function() {
        if(confirm('Remove Selected expense?')){
            etrack.expenses.splice(this.dataset.id, 1);
            //save list
            etrack.save();

            //Reload List
            etrack.create();
        }
    },
    //Initial function to input the selected expense
    edit : function () {
        
        //output id variables to be shown to user
        document.getElementById('exp-item').value=etrack.expenses[this.dataset.id].name;
        document.getElementById('exp-amount').value=etrack.expenses[this.dataset.id].amount;
        
        etrack.hEditId = this.dataset.id ;

        //toggle button
        etrack.toggleEdit();
       
    },

    //editing current selected expense
    editUnits : function(evt){
        
        //Prevent form sumbit
        evt.preventDefault();

        //input new values
        etrack.expenses[etrack.hEditId].name = etrack.hExpense.value;
        etrack.expenses[etrack.hEditId].amount = etrack.hAmount.value;
        etrack.hExpense.value = '';
        etrack.hAmount.value = '';

        //save list
        etrack.save();
        
        // reload List
        etrack.create();

        //toggle button
        etrack.toggleEdit();
    },

    
    //toggle the HTML Edit & Add inputers
    toggleEdit : function(){
        var x = document.getElementById("exp-add-btn");
        // toggle from add -> edit-> add -> etc.
        if (x.style.display === "none") {
            x.style.display = "block";
            document.getElementById("exp-add").style.display="block";
            document.getElementById("exp-edit-btn").style.display="none";
            document.getElementById("exp-edit").style.display="none";
        } else {
            x.style.display = "none";
            document.getElementById("exp-add").style.display="none";
            document.getElementById("exp-edit-btn").style.display="block";
            document.getElementById("exp-edit").style.display="block";
        
        }
    },

    //saving expense list to local storage
    save : function(){
    if(localStorage.expenses == undefined){localStorage.expenses = "[]";} 
    localStorage.expenses = JSON.stringify(etrack.expenses); // save on local server as a json.stringify-ied array
  }
    
}
window.addEventListener('DOMContentLoaded', etrack.init)